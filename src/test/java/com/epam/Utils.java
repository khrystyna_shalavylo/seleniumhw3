package com.epam;

public class Utils {

    public static final String WEBDRIVER = "webdriver.chrome.driver";
    public static final String WEBDRIVER_PATH = "src/main/resources/chromedriver.exe";
    public static final String GMAIL_URL = "https://accounts.google.com/ServiceLogin/signinchooser?service=mail&passive=true&rm=false&continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&ss=1&scc=1&ltmpl=default&ltmplcache=2&emr=1&osid=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin";
    public static final String LOGIN_1= "khrystynasha1@gmail.com";
    public static final String LOGIN_2 = "khrystynasha2@gmail.com";
    public static final String LOGIN_3 = "khrystynasha3@gmail.com";
    public static final String PASSWORD = "q1w2e3w2q1";
}
