package com.epam;

import com.epam.businessobjects.LoginBO;
import com.epam.businessobjects.WorkWithMailBO;
import com.epam.model.Message;
import com.epam.model.User;
import com.epam.providers.DriverProvider;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.Iterator;
import java.util.stream.Stream;

import static com.epam.Utils.*;
import static com.epam.utils.Const.*;

public class TestWorkWithMail {
    @BeforeClass
    public void setUp() {
        System.setProperty(WEBDRIVER, WEBDRIVER_PATH);
    }

    @DataProvider(parallel = true)
    public Iterator<Object[]> users() {
        return Stream.of(
                new Object[]{new User(LOGIN_1, PASSWORD)},
                new Object[]{new User(LOGIN_2, PASSWORD)},
                new Object[]{new User(LOGIN_3, PASSWORD)}
        ).iterator();
    }

    @Test(dataProvider = "users")
    public void testMailWork(User user) {
        Message message = new Message(RECIPIENT, CC, BCC, SUBJECT, MESSAGE);
        LoginBO loginBusinessObject = new LoginBO();
        DriverProvider.getDriver().get(GMAIL_URL);
        loginBusinessObject.login(user);
        WorkWithMailBO workWithMailBO = new WorkWithMailBO();
        workWithMailBO.composeMessage(message);
        workWithMailBO.saveMessageAndClose();
        workWithMailBO.openLastDraft();
        Assert.assertTrue(workWithMailBO.isLastDraftEqualsToCreatedMessage(message), "Information in draft was not saved.");
        workWithMailBO.sendMessageFromDrafts();
    }

    @AfterClass
    public void end() {
        DriverProvider.getDriver().close();
    }
}
