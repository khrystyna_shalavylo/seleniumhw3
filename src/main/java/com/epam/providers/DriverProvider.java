package com.epam.providers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import static com.epam.utils.Const.IMPLICITY_WAIT ;

public class DriverProvider {

    private static ThreadLocal<WebDriver> driver = new ThreadLocal<>();

    public DriverProvider() {
    }

    public static WebDriver getDriver() {
        if (Objects.isNull(driver.get())) {
            driver.set(new ChromeDriver());
            setUpDriver(driver);
        }
        return driver.get();
    }

    private static void setUpDriver(ThreadLocal<WebDriver> driver) {
        driver.get().manage().timeouts().implicitlyWait(IMPLICITY_WAIT, TimeUnit.SECONDS);
        driver.get().manage().window().maximize();
    }

    public static void close() {
        if (!Objects.isNull(driver.get())) {
            driver.get().quit();
            driver.remove();
        }
    }
}
