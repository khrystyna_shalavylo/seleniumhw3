package com.epam.providers;

import org.openqa.selenium.ElementNotInteractableException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static com.epam.utils.Const.SLEEP_TIME;
import static com.epam.utils.Const.TIME_OUT;

public class WaitProvider {

    public WaitProvider() {
    }

    public WebElement waitForElement(WebElement element) {
        return new WebDriverWait(DriverProvider.getDriver(), TIME_OUT, SLEEP_TIME)
                .ignoring(StaleElementReferenceException.class, ElementNotInteractableException.class)
                .until(ExpectedConditions.elementToBeClickable(element));
    }
}
