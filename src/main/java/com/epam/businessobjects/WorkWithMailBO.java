package com.epam.businessobjects;

import com.epam.model.Message;
import com.epam.pageobjects.HomePage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WorkWithMailBO {
    private HomePage homePage;
    private static Logger logger = LogManager.getLogger(WorkWithMailBO.class);

    public WorkWithMailBO() {
        homePage = new HomePage();
    }

    public void composeMessage(Message message) {
        logger.info("Pressing compose button.");
        homePage.getCompose().click();
        logger.info("Entering recipient.");
        homePage.getFieldTo().sendKeys(message.getRecipient());
        logger.info("Entering CC.");
        homePage.getFieldCC().sendKeys(message.getCC());
        logger.info("Entering BCC.");
        homePage.getFieldBCC().sendKeys(message.getBCC());
        logger.info("Entering subject.");
        homePage.getFieldSubject().sendKeys(message.getSubject());
        logger.info("Entering message text.");
        homePage.getFieldMessage().sendKeys(message.getMessageText());
    }

    public void saveMessageAndClose() {
        logger.info("Saving message and pressing close button.");
        homePage.getSaveAndClose().click();
    }

    public void openLastDraft() {
        logger.info("Pressing drafts button.");
        homePage.getDrafts().click();
        logger.info("Open last draft.");
        homePage.getLastDraft().click();
    }

    public boolean isLastDraftEqualsToCreatedMessage(Message message) {
        logger.info("Comparing last draft message with created message.");
    return message.getRecipient().equals(homePage.getFieldTo().getText()) &&
            message.getCC().equals(homePage.getFieldCC().getText()) &&
            message.getBCC().equals(homePage.getFieldBCC().getText()) &&
            message.getSubject().equals(homePage.getFieldSubject().getText()) &&
            message.getMessageText().equals(homePage.getFieldMessage().getText());
    }

    public void sendMessageFromDrafts() {
        logger.info("Sending message.");
        homePage.getSend().click();
    }
}
