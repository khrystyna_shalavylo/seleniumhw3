package com.epam.businessobjects;

import com.epam.model.User;
import com.epam.pageobjects.LoginPage;
import com.epam.pageobjects.PasswordPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class LoginBO {
    private LoginPage loginPage;
    private PasswordPage passwordPage;
    private static Logger logger = LogManager.getLogger(LoginBO.class);

    public LoginBO() {
        loginPage = new LoginPage();
        passwordPage = new PasswordPage();
    }

    public void login(User user) {
        enterLogin(user.getLogin());
        pressLoginNext();
        enterPassword(user.getPassword());
        pressPasswordNext();
    }

    private void enterLogin(String login) {
        logger.info("User enters login.");
        loginPage.getLoginInput().sendKeys(login);
    }

    private void pressLoginNext(){
        logger.info("User presses next button after login.");
        loginPage.getNextButton().click();
    }

    private void enterPassword(String password) {
        logger.info("User enters password.");
        passwordPage.getPassInput().sendKeys(password);
    }

    private void pressPasswordNext(){
        logger.info("User presses next button after password.");
        passwordPage.getNextButton().click();
    }
}
