package com.epam.model;

import java.util.Objects;

public class Message {

    private String recipient;
    private String cc;
    private String bcc;
    private String subject;
    private String messageText;

    public Message() {
    }

    public Message(String recipient, String cc, String bcc, String subject, String messageText) {
        this.recipient = recipient;
        this.cc = cc;
        this.bcc = bcc;
        this.subject = subject;
        this.messageText = messageText;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getCC() {
        return cc;
    }

    public void setCC(String cc) {
        this.cc = cc;
    }

    public String getBCC() {
        return bcc;
    }

    public void setBCC(String bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Message)) {
            return false;
        }
        Message message = (Message) o;
        return Objects.equals(recipient, message.recipient) &&
                Objects.equals(cc, message.cc) &&
                Objects.equals(bcc, message.bcc) &&
                Objects.equals(subject, message.subject) &&
                Objects.equals(messageText, message.messageText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recipient, cc, bcc, subject, messageText);
    }

    @Override
    public String toString() {
        return "Message: \n" +
                "Recipient: '" + recipient + "\n" +
                "CC: " + cc + "\n" +
                "BCC: " + bcc + "\n" +
                "Subject: '" + subject + "\n" +
                "Message Text: " + messageText + "\n";
    }
}
