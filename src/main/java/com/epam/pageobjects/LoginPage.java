package com.epam.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends PageObject {

    @FindBy(id = "identifierId")
    private WebElement loginInput;

    @FindBy(id = "identifierNext")
    private WebElement nextButton;

    public WebElement getLoginInput() {
        return loginInput;
    }

    public WebElement getNextButton() {
        return nextButton;
    }
}
