package com.epam.pageobjects;

import com.epam.providers.DriverProvider;
import org.openqa.selenium.support.PageFactory;

public class PageObject {

    public PageObject() {
        PageFactory.initElements(DriverProvider.getDriver(), this);
    }
}
