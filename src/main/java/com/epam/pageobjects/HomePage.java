package com.epam.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage {
    @FindBy(css = "div.T-I.J-J5-Ji.T-I-KE.L3")
    private WebElement compose;

    @FindBy(id = ":r1")
    private WebElement fieldTo;

    @FindBy(id = ":r2")
    private WebElement fieldCC;

    @FindBy(id = ":r3")
    private WebElement fieldBCC;

    @FindBy(id = ":qj")
    private WebElement fieldSubject;

    @FindBy(id = ":ro")
    private WebElement fieldMessage;

    @FindBy(id = ":nk")
    private WebElement saveAndClose;

    @FindBy(id=":wo")
    private WebElement send;

    @FindBy(id = ":ls")
    private WebElement drafts;

    @FindBy(id=":rk")
    private WebElement lastDraft;


    public WebElement getCompose() {
        return compose;
    }

    public WebElement getFieldTo() {
        return fieldTo;
    }
    public WebElement getFieldCC() {
        return fieldCC;
    }

    public WebElement getFieldBCC() {
        return fieldBCC;
    }

    public WebElement getFieldSubject() {
        return fieldSubject;
    }

    public WebElement getFieldMessage() {
        return fieldMessage;
    }

    public WebElement getSaveAndClose() {
        return saveAndClose;
    }

    public WebElement getSend() {
        return send;
    }

    public WebElement getDrafts() {
        return drafts;
    }

    public WebElement getLastDraft() {
        return lastDraft;
    }
}
