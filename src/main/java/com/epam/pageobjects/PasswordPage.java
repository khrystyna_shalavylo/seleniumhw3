package com.epam.pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PasswordPage {
    @FindBy(id = "password")
    private WebElement passInput;

    @FindBy(id = "passwordNext")
    private WebElement nextPassButton;

    public WebElement getPassInput() {
        return passInput;
    }

    public WebElement getNextButton() {
        return nextPassButton;
    }
}
