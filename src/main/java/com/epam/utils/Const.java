package com.epam.utils;

public class Const {
    public final static int TIME_OUT = 150;
    public final static int SLEEP_TIME = 2000;
    public final static int IMPLICITY_WAIT = 300;
    public static final String RECIPIENT = "shalavilo52@gmail.com";
    public static final String CC = "shalavilo@live.com";
    public static final String BCC = "khrystyna.shalavylo.kn.2016@lpnu.ua";
    public static final String SUBJECT = "Test";
    public static final String MESSAGE = "This is test letter.";

    public Const() {
    }
}
